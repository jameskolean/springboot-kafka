package com.codegreenllc.kafka.services;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class Consumer {
	@KafkaListener(topics = "test", groupId = "group_id")
	public void consume(final String message) {
		log.info(String.format("$$ -> Consumed Message -> %s", message));
	}
}