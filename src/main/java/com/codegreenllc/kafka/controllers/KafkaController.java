package com.codegreenllc.kafka.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codegreenllc.kafka.services.Producer;

@RestController
public class KafkaController {
	@Autowired
	private Producer producer;

	@PostMapping(value = "/kafka/publish")
	public void sendMessageToKafkaTopic(@RequestParam("message") final String message) {
		producer.sendMessage(message);
	}
}